#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int main() {
    int N = 10;
    vector<long> shuffled(N);
    for(int i = 0; i < N; i++)
        shuffled[i] = i;
    std::vector<long>::iterator it=shuffled.begin();
    it++;
    random_shuffle(it, shuffled.end());
    for(int i = 0; i < shuffled.size(); i++)
        cout<<shuffled[i]<<endl;
    return 0;
}



